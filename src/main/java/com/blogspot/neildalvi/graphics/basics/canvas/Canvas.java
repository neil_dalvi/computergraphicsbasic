package com.blogspot.neildalvi.graphics.basics.canvas;

import com.blogspot.neildalvi.graphics.basics.client.Error;

public class Canvas {

	private char[][] canvasBoard;
	private int width;
	private int height;

	public Canvas(int width, int height) {

		if (width * height > 1225000000) {
			throw new IllegalArgumentException(Error.CANVAS_AREA_CONSTRAINT.getMsg());
		}

		canvasBoard = new char[height + 2][width + 2];
		this.width = width;
		this.height = height;
		initBoard();
	}

	private void initBoard() {
		for (int j = 0; j <= this.width + 1; j++) {
			canvasBoard[0][j] = '-';
			canvasBoard[this.height + 1][j] = '-';
		}
		for (int j = 1; j <= this.height; j++) {
			canvasBoard[j][0] = '|';
			canvasBoard[j][this.width + 1] = '|';
		}
		clearBoard();
	}

	private void clearBoard() {
		for (int i = 1; i <= this.height; i++) {
			for (int j = 1; j <= this.width; j++) {
				canvasBoard[i][j] = ' ';
			}
		}
	}

	public void updatePixel(int y, int x, char color) {
		this.canvasBoard[y][x] = color;
	}

	public void drawLine(int[][] pts, char color) {
		for (int i = 0; i < pts.length; i++) {
			this.updatePixel(pts[i][1], pts[i][0], color);
		}
	}

	public void fillColor(int x, int y, char color) {
		if (this.canvasBoard[y][x] != color
				&& !(this.canvasBoard[y][x] == 'x' || this.canvasBoard[y][x] == '-' || this.canvasBoard[y][x] == '|')) {
			this.canvasBoard[y][x] = color;

			fillColor(x + 1, y - 1, color);
			fillColor(x + 1, y, color);
			fillColor(x + 1, y + 1, color);
			fillColor(x, y + 1, color);
			fillColor(x - 1, y + 1, color);
			fillColor(x - 1, y, color);
			fillColor(x - 1, y - 1, color);
			fillColor(x, y - 1, color);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(this.width * this.height);
		for (int i = 0; i <= this.height + 1; i++) {
			for (int j = 0; j <= this.width + 1; j++) {
				sb.append(this.canvasBoard[i][j]);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
}
