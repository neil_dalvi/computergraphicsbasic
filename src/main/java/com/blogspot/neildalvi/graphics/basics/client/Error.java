package com.blogspot.neildalvi.graphics.basics.client;

public enum Error {

	 CANVAS_AREA_CONSTRAINT("Total Area of canvas should not be more than 1225000000"),
	 SLANT_LINE_UNSUPPORTED("Slant line is not supported."),
	 NEGATIVE_OR_ZERO_COORDINATES("Values cannot be negative or zero"),
	// Input related message
	 INVALID_COMMAND("Invalid command"),
	 COLOR_BE_SINGLE_CHARACTER("Color should be a single character"),
	 PROGRAM_END_MESSAGE("Program Quiting\nThank you!"),
	 EXPECTED_4_INPUTS("Expected 4 arguments x1 y1 x2 y2"),
	 EXPECTED_3_INPUTS("Expected 3 arguments x y color-character"),
	 EXPECTED_WIDTH_HEIGHT_INPUTS("Expected 2 arguments width height"),
	 EXPECTED_INTEGER_INPUT("Expected Integer inputs"),
	 UNKWON_ERROR("Unknown Error occured.\n"),
	 UNINITIALIZED_CANVAS("Initialize the Canvas: C width height\n");
	 
	 String msg;
	
	Error(String msg) {
		this.msg = msg;
	}
	
	public String getMsg() {
		return this.msg;
	}
	
}
