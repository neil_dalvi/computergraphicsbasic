package com.blogspot.neildalvi.graphics.basics.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.blogspot.neildalvi.graphics.basics.logic.DrawLogic;

public class MainClient {

	public static void main(String[] args) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		DrawLogic logic = null;
		executeCommand(br, logic);
	}

	private static void executeCommand(BufferedReader br, DrawLogic logic) throws IOException {
		String[] word;
		while ((word = br.readLine().split(" ")) != null) {

			if (!execute(logic, word)) {
				break;
			}
		}
	}

	protected static boolean execute(DrawLogic logic, String[] word) {
		try {

			if (word[0].equals("C")) {
				if (!validateWidthAndHeightInputParam(word)) {
					return true;
				}
				int width = Integer.parseInt(word[1]);
				int height = Integer.parseInt(word[2]);
				if (!validateInputs(width, height)) {
					return true;
				}
				logic = new DrawLogic(width, height);
			} else if (word[0].equals("L")) {
				if (!validateLogicInitialized(logic) || !validateFourInputParam(word)) {
					return true;
				}
				int x1 = Integer.parseInt(word[1]);
				int y1 = Integer.parseInt(word[2]);
				int x2 = Integer.parseInt(word[3]);
				int y2 = Integer.parseInt(word[4]);
				logic.drawLine(x1, y1, x2, y2, 'x');
			} else if (word[0].equals("R")) {
				if (!validateLogicInitialized(logic) || !validateFourInputParam(word)) {
					return true;
				}
				int x1 = Integer.parseInt(word[1]);
				int y1 = Integer.parseInt(word[2]);
				int x2 = Integer.parseInt(word[3]);
				int y2 = Integer.parseInt(word[4]);
				logic.drawRectangle(x1, y1, x2, y2, 'x');
			} else if (word[0].equals("B")) {
				if (!validateLogicInitialized(logic) || !validateThreeInputParam(word)) {
					return true;
				}
				int x = Integer.parseInt(word[1]);
				int y = Integer.parseInt(word[2]);
				if (!validateInputs(x, y) || !validateColor(word[3])) {
					return true;
				}
				char color = word[3].charAt(0);
				logic.fillColor(x, y, color);
			} else if (word[0].equals("Q")) {
				System.out.println(Error.PROGRAM_END_MESSAGE.getMsg());
				return false;
			} else {
				System.out.println(Error.INVALID_COMMAND.getMsg());
			}

		} catch (NumberFormatException e) {
			System.out.println(Error.EXPECTED_INTEGER_INPUT.getMsg());
		} catch (Exception e) {
			System.out.println(Error.UNKWON_ERROR + Error.PROGRAM_END_MESSAGE.getMsg());
		}
		return true;
	}

	private static boolean validateFourInputParam(String[] words) {
		if (words.length != 5) {
			System.out.println(Error.EXPECTED_4_INPUTS.getMsg());
			return false;
		}
		return true;
	}

	private static boolean validateWidthAndHeightInputParam(String[] words) {
		if (words.length != 3) {
			System.out.println(Error.EXPECTED_WIDTH_HEIGHT_INPUTS.getMsg());
			return false;
		}
		return true;
	}

	private static boolean validateThreeInputParam(String[] words) {
		if (words.length != 4) {
			System.out.println(Error.EXPECTED_3_INPUTS.getMsg());
			return false;
		}
		return true;
	}

	private static boolean validateInputs(int x1, int y1) {
		if (x1 <= 0 || y1 <= 0) {
			System.out.println(Error.NEGATIVE_OR_ZERO_COORDINATES.getMsg());
			return false;
		}
		return true;
	}

	private static boolean validateColor(String color) {
		if (color.length() != 1) {
			System.out.println(Error.COLOR_BE_SINGLE_CHARACTER.getMsg());
			return false;
		}
		return true;
	}

	private static boolean validateLogicInitialized(DrawLogic logic) {
		if (logic == null) {
			System.out.println(Error.UNINITIALIZED_CANVAS.getMsg());
			return false;
		}
		return true;
	}
}
