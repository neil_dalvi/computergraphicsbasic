package com.blogspot.neildalvi.graphics.basics.logic;

import com.blogspot.neildalvi.graphics.basics.canvas.Canvas;
import com.blogspot.neildalvi.graphics.basics.shape.Line;
import com.blogspot.neildalvi.graphics.basics.shape.Rectangle;

public class DrawLogic {

	Canvas canvas;
	Line line;
	Rectangle rectangle;

	public DrawLogic(int width, int height) {
		canvas = new Canvas(width, height);
		line = new Line();
		rectangle = new Rectangle();
		System.out.println(canvas);
	}

	public void drawLine(int x1, int y1, int x2, int y2, char color) {
		int[][] pts = line.draw(x1, y1, x2, y2);
		canvas.drawLine(pts, color);
		System.out.println(canvas);
	}

	public void drawRectangle(int x1, int y1, int x2, int y2, char color) {
		int[][][] pts = rectangle.draw(x1, y1, x2, y2, line);
		for (int i = 0; i < pts.length; i++) {
			canvas.drawLine(pts[i], color);
		}
		System.out.println(canvas);
	}

	public void fillColor(int x, int y, char color) {
		canvas.fillColor(x, y, color);
		System.out.println(canvas);
	}
}
