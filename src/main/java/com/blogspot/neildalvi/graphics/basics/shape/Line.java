package com.blogspot.neildalvi.graphics.basics.shape;

import com.blogspot.neildalvi.graphics.basics.client.Error;

public class Line {

	public int[][] draw(int x1, int y1, int x2, int y2) {

		if (x1 == x2) {
			int[][] pts = new int[Math.abs(y1 - y2) + 1][2];
			int min = Math.min(y1, y2);
			for (int i = 0; i < pts.length; i++) {
				pts[i][0] = x1;
				pts[i][1] = min;
				min++;
			}
			return pts;
		} else if (y1 == y2) {
			int[][] pts = new int[Math.abs(x1 - x2) + 1][2];
			int min = Math.min(x1, x2);
			for (int i = 0; i < pts.length; i++) {
				pts[i][0] = min;
				pts[i][1] = y1;
				min++;
			}
			return pts;
		} else {
			throw new IllegalArgumentException(Error.SLANT_LINE_UNSUPPORTED.getMsg());
		}
	}

}
