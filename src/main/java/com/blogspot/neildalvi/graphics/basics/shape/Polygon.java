package com.blogspot.neildalvi.graphics.basics.shape;

public class Polygon {

	public static int[][][] draw(int[][] pts, Line line) {
		int result[][][] = new int[pts.length][][];
		for (int i = 0; i < pts.length - 1; i++) {
			result[i] = line.draw(pts[i][0], pts[i][1], pts[i + 1][0], pts[i + 1][1]);
		}
		result[pts.length - 1] = line.draw(pts[pts.length - 1][0], pts[pts.length - 1][1], pts[0][0], pts[0][1]);
		return result;
	}

}
