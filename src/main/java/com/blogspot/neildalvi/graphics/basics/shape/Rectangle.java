package com.blogspot.neildalvi.graphics.basics.shape;

public class Rectangle {

	public int[][][] draw(int x1, int y1, int x2, int y2, Line line) {
		int[][] pts = new int[4][2];
		pts[0][0] = x1;
		pts[0][1] = y1;

		pts[1][0] = x1;
		pts[1][1] = y2;

		pts[2][0] = x2;
		pts[2][1] = y2;

		pts[3][0] = x2;
		pts[3][1] = y1;
		return Polygon.draw(pts, line);
	}

}
