package com.blogspot.neildalvi.graphics.basics.canvas;

import static junit.framework.Assert.fail;

import org.junit.Test;
import com.blogspot.neildalvi.graphics.basics.client.Error;


import static junit.framework.Assert.assertEquals;

public class CanvasTest  {
	
	@Test
	public void testCanvasOutOfBounds() {
		try {
			new Canvas(35000,35001);
			fail("Should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals(Error.CANVAS_AREA_CONSTRAINT.getMsg(), e.getMessage());
		}
	}
	
}
