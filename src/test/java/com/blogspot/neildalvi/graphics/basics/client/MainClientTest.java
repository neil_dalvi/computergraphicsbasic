package com.blogspot.neildalvi.graphics.basics.client;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyChar;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import com.blogspot.neildalvi.graphics.basics.logic.DrawLogic;

public class MainClientTest {

	@Mock
	private DrawLogic logic;

	@Before
	public void setup() {
		logic = Mockito.mock(DrawLogic.class);
		Mockito.doNothing().when(logic).drawLine(anyInt(), anyInt(), anyInt(), anyInt(), anyChar());
		Mockito.doNothing().when(logic).drawRectangle(anyInt(), anyInt(), anyInt(), anyInt(), anyChar());
		Mockito.doNothing().when(logic).fillColor(anyInt(), anyInt(), anyChar());
	}

	@Test
	public void testMainForNegativeOrZeroInputs() throws IOException {
		checkNegativeInput("C -1 1");
		checkNegativeInput("C 1 -1");
		checkNegativeInput("C 0 1");
		checkNegativeInput("C 1 0");

		checkNegativeInput("B -1 1 'c'");
		checkNegativeInput("B 1 -1 'c'");
		checkNegativeInput("B 0 1 'c'");
		checkNegativeInput("B 1 0 'c'");

		checkNegativeLineInputs("L -1 1 1 1");
		checkNegativeLineInputs("L 1 -1 1 1");
		checkNegativeLineInputs("L 1 1 -1 1");
		checkNegativeLineInputs("L 1 1 1 -1");

		checkNegativeLineInputs("L 0 1 1 1");
		checkNegativeLineInputs("L 1 0 1 1");
		checkNegativeLineInputs("L 1 1 0 1");
		checkNegativeLineInputs("L 1 1 1 0");

		checkNegativeLineInputs("R -1 1 1 1");
		checkNegativeLineInputs("R 1 -1 1 1");
		checkNegativeLineInputs("R 1 1 -1 1");
		checkNegativeLineInputs("R 1 1 1 -1");

		checkNegativeLineInputs("R 0 1 1 1");
		checkNegativeLineInputs("R 1 0 1 1");
		checkNegativeLineInputs("R 1 1 0 1");
		checkNegativeLineInputs("R 1 1 1 0");
	}

	@Test
	public void testMainForNonNumberInput() throws IOException {
		checkNonNumericInputs("C A 1");
		checkNonNumericInputs("C 1 A");

		checkNonNumericInputs("B A 1 'c'");
		checkNonNumericInputs("B 1 A 'c'");

		checkNonNumericInputs("L A 1 1 1");
		checkNonNumericInputs("L 1 A 1 1");
		checkNonNumericInputs("L 1 1 A 1");
		checkNonNumericInputs("L 1 1 1 A");

		checkNonNumericInputs("R A 1 1 1");
		checkNonNumericInputs("R 1 A 1 1");
		checkNonNumericInputs("R 1 1 A 1");
		checkNonNumericInputs("R 1 1 1 A");
	}

	@Test
	public void testMainForCallWithoutCanvasInitialization() throws IOException {
		logic = null;
		checkUninitializedCanvas("L 1 1 1 1");
		checkUninitializedCanvas("R 1 1 1 1");
		checkUninitializedCanvas("B 1 1");
	}

	@Test
	public void testMainForValidCalls() throws IOException {
		MainClient.execute(logic, "L 1 1 1 1".split(" "));
		verify(logic).drawLine(1, 1, 1, 1, 'x');
		MainClient.execute(logic, "R 1 1 1 1".split(" "));
		verify(logic).drawRectangle(1, 1, 1, 1, 'x');
		MainClient.execute(logic, "B 1 1 b".split(" "));
		verify(logic).fillColor(1, 1, 'b');
		// MainClient.execute(logic, "C 1 1".split(" "));
		// verify(logic)
	}

	@Test
	public void testQuitMethod() throws IOException {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		System.setOut(new PrintStream(bo));
		boolean actual = MainClient.execute(logic, "Q".split(" "));
		assertEquals(false, actual);
		bo.flush();
		String allWrittenLines = new String(bo.toByteArray());
		assertTrue(allWrittenLines.contains(Error.PROGRAM_END_MESSAGE.getMsg()));
	}

	private void checkUninitializedCanvas(String line) throws IOException {

		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		System.setOut(new PrintStream(bo));
		boolean actual = MainClient.execute(logic, line.split(" "));
		assertEquals(true, actual);
		bo.flush();
		String allWrittenLines = new String(bo.toByteArray());
		assertTrue(allWrittenLines.contains(Error.UNINITIALIZED_CANVAS.getMsg()));
	}

	private void checkNonNumericInputs(String line) throws IOException {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		System.setOut(new PrintStream(bo));
		boolean actual = MainClient.execute(logic, line.split(" "));
		assertEquals(true, actual);
		bo.flush();
		String allWrittenLines = new String(bo.toByteArray());
		assertTrue(allWrittenLines.contains(Error.EXPECTED_INTEGER_INPUT.getMsg()));
	}

	@Test
	public void testMainForUnknow() {
	}

	private void checkNegativeInput(String line) throws IOException {
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		System.setOut(new PrintStream(bo));
		boolean actual = MainClient.execute(logic, line.split(" "));
		assertEquals(true, actual);
		bo.flush();
		String allWrittenLines = new String(bo.toByteArray());
		assertTrue(allWrittenLines.contains(Error.NEGATIVE_OR_ZERO_COORDINATES.getMsg()));
	}

	private void checkNegativeLineInputs(String line) throws IOException {
		ByteArrayOutputStream bo;
		boolean actual;
		String allWrittenLines;
		bo = new ByteArrayOutputStream();
		System.setOut(new PrintStream(bo));
		actual = MainClient.execute(logic, line.split(" "));
		assertEquals(true, actual);
		bo.flush();
		allWrittenLines = new String(bo.toByteArray());
		assertTrue(!allWrittenLines.contains(Error.NEGATIVE_OR_ZERO_COORDINATES.getMsg()));
	}

}
