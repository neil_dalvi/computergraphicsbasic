package com.blogspot.neildalvi.graphics.basics.logic;

import static org.junit.Assert.*;

import org.junit.Test;

public class DrawLogicTest {

	@Test
	public void testGivenExample() {
		DrawLogic logic = new DrawLogic(20, 4);
		logic.drawLine(1, 2, 6, 2, 'x');
		logic.drawLine(6, 3, 6, 4, 'x');
		logic.drawRectangle(14, 1, 18, 3, 'x');
		logic.fillColor(10, 3, 'o');
		String expected = "" + "----------------------\n|oooooooooooooxxxxxoo|\n|xxxxxxooooooox   xoo|\n|     xoooooooxxxxxoo|\n|     xoooooooooooooo|\n----------------------\n";
		String actual = logic.canvas.toString();
		System.out.println(expected);
		System.out.println(actual);
		assertEquals(expected, actual);
	}

}
