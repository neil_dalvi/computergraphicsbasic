package com.blogspot.neildalvi.graphics.basics.shape;

import static org.junit.Assert.*;

import org.junit.Test;
import com.blogspot.neildalvi.graphics.basics.client.Error;

import com.blogspot.neildalvi.graphics.basics.canvas.Canvas;

public class LineTest {

	@Test
	public void testDrawVerticalLine() {
		int[][] actuals = new Line().draw(3, 3, 3, 6);
		int[][] expecteds = new int[][] { { 3, 3 }, { 3, 4 }, { 3, 5 }, { 3, 6 } };
		assertArrayEquals(expecteds, actuals);
	}

	@Test
	public void testDrawHorizontalLine() {
		int[][] actuals = new Line().draw(3, 3, 6, 3);
		int[][] expecteds = new int[][] { { 3, 3 }, { 4, 3 }, { 5, 3 }, { 6, 3 } };
		assertArrayEquals(expecteds, actuals);
	}

	@Test
	public void testDrawSlantLine() {
		try {
			new Line().draw(0, 0, 1, 1);
			fail("Should have thrown exception");
		} catch (IllegalArgumentException e) {
			assertEquals(e.getMessage(), Error.SLANT_LINE_UNSUPPORTED.getMsg());
		}
	}

}
